CREATE TABLE IF NOT EXISTS workflow.t_state (
  name varchar(255) NOT NULL,
  PRIMARY KEY (name)
)CHARACTER SET=utf8mb4;

CREATE TABLE IF NOT EXISTS workflow.t_edge (
  from_state varchar(255) NOT NULL,
  to_state varchar(255) NOT NULL,
  action_name varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (from_state, to_state),
  CONSTRAINT from_state_fk FOREIGN KEY (from_state) REFERENCES t_state(name),
  CONSTRAINT to_state_fk FOREIGN KEY (to_state) REFERENCES t_state(name)
)CHARACTER SET=utf8mb4;

CREATE TABLE IF NOT EXISTS workflow.t_user (
  user_id INT NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (user_id)
)CHARACTER SET=utf8mb4;

CREATE TABLE IF NOT EXISTS workflow.t_issue (
  issue_id INT NOT NULL AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  PRIMARY KEY (issue_id),
  CONSTRAINT state_fk FOREIGN KEY (state) REFERENCES t_state(name)
)CHARACTER SET=utf8mb4;

CREATE TABLE IF NOT EXISTS workflow.t_user_issue (
  user_pk INT NOT NULL,
  issue_pk INT NOT NULL,
  PRIMARY KEY (user_pk, issue_pk),
  CONSTRAINT user_pk_fk FOREIGN KEY (user_pk) REFERENCES t_user(user_id),
  CONSTRAINT issue_pk_fk FOREIGN KEY (issue_pk) REFERENCES t_issue(issue_id)
)CHARACTER SET=utf8mb4;

INSERT INTO workflow.t_state (name) VALUES
    ("open"),
    ("resolved"),
    ("in progress"),
    ("closed"),
    ("eden"),
    ("reopened");

INSERT INTO workflow.t_edge (from_state, to_state, name) VALUES
    ("eden", "open", "create"),
    ("open", "in progress", "start"),
    ("open", "resolved", "resolve"),
    ("in progress", "open", "stop"),
    ("resolved", "reopened", "reopen"),
    ("reopened", "resolved", "resolve"),
    ("reopened", "in progress", "start"),
    ("reopened", "closed", "close"),
    ("in progress", "resolved", "resolve"),
    ("in progress", "closed", "close"),
    ("closed", "reopened", "reopen"),
    ("open", "closed", "close");

INSERT INTO workflow.t_user (user_id, name) VALUES
        (1, "bob"),
        (2, "alice");

INSERT INTO workflow.t_issue (issue_id, title, state) VALUES
    (1, "Bad client bug", "open"),
    (2, "Database key is in plaintext", "closed"),
    (3, "I lost my computer", "resolved");

INSERT INTO workflow.t_user_issue (user_pk, issue_pk) VALUES
        (1, 1),
        (1, 2),
        (2, 3);