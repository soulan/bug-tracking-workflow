package com.behavox.workflow.service;

import com.behavox.workflow.model.EdgeEntity;
import com.behavox.workflow.model.IssueEntity;
import com.behavox.workflow.model.StateEntity;
import com.behavox.workflow.model.UserIssueEntity;
import com.behavox.workflow.persist.IssueRepository;
import com.behavox.workflow.persist.StateRepository;
import com.behavox.workflow.persist.UserIssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class IssueService {

	@Autowired
	UserIssueRepository userIssueRepository;

	@Autowired
	IssueRepository issueRepository;

	@Autowired
	StateRepository stateRepository;


	public List<UserIssueEntity> getAllUserIssue(){
		return this.userIssueRepository.findAll();
	}

	public IssueEntity moveIssueToAnotherState(long issueId, String desiredState) {
		checkNotNull(desiredState);

		Optional<IssueEntity> issueEntity = issueRepository.findById(issueId);
		if(!issueEntity.isPresent()) {
			throw new MoveIssueException(String.format("Issue %s does not exist!", issueId));
		}

		Optional<StateEntity> desiredStateEntity = stateRepository.findById(desiredState);
		if(!desiredStateEntity.isPresent()) {
			throw new MoveIssueException(String.format("State %s does not exist!", desiredState));
		}

		StateEntity currentState = stateRepository.findById(issueEntity.get().getState())
				.get();

		Set<String> legalMoves = currentState.getOutgoingEdgeEntities()
				.stream()
				.map(EdgeEntity::getTo)
				.collect(Collectors.toSet());

		if(!legalMoves.contains(desiredState)){
			throw new MoveIssueException(String.format("Cannot move issue %s to state %s, possible moves are: %s", issueId, desiredState, legalMoves));
		}

		IssueEntity entity = issueEntity.get();
		entity.setState(desiredState);

		IssueEntity updated;
		try {
			updated = this.issueRepository.save(entity);
		} catch(DataIntegrityViolationException dive) {
			throw new MoveIssueException(String.format("Cannot move %s to state %s", issueId, desiredState));
		}

		return updated;
	}

}
