package com.behavox.workflow.service;

public class MoveIssueException extends RuntimeException {
	public MoveIssueException(String message, Throwable cause){
		super(message, cause);
	}
	public MoveIssueException(String message){
		super(message);
	}
}
