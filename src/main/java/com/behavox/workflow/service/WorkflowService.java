package com.behavox.workflow.service;

import com.behavox.workflow.model.EdgeEntity;
import com.behavox.workflow.model.StateEntity;

import com.behavox.workflow.persist.EdgeRepository;
import com.behavox.workflow.persist.StateRepository;

import static com.google.common.base.Preconditions.checkNotNull;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkflowService {

	@Autowired
	private EdgeRepository edgeRepository;

	@Autowired
	private StateRepository stateRepository;

	/**
	 * Fetches the entire graph with the states and all of their connected edges.
	 * @return
	 */
	public List<StateEntity> getWorkFlow(){
		return stateRepository.findAll();
	}

	public void addEdge(String from, String to, String name, String action) throws WorkflowException  {
		checkNotNull(from, to, name, action);
		EdgeEntity existingEdge = this.edgeRepository.findFirstByFromAndTo(from, to);
		if(existingEdge != null){
			String message = String.format("Edge already exists from %s to %s with name %s and action %s",
					from,
					to,
					name,
					action);

			throw new WorkflowException(message);
		}

		EdgeEntity edgeEntity = new EdgeEntity(from, to, name, action);
		try {
			this.edgeRepository.save(edgeEntity);
		} catch(DataIntegrityViolationException dive) {
			throw new WorkflowException("Unable to create edge, ensure that the states are created and are valid.", dive);
		}
	}

	public void addState(String state) throws WorkflowException {
		checkNotNull(state);
		StateEntity stateEntity = new StateEntity(state);
		this.stateRepository.save(stateEntity);
	}

	public void removeState(String state) throws WorkflowException {
		checkNotNull(state);

		StateEntity stateEntity = new StateEntity(state);
		try {
			this.stateRepository.delete(stateEntity);
		} catch(DataIntegrityViolationException dive) {
			throw new WorkflowException("Unable to delete state, ensure state is not connected to any edges.", dive);
		}
	}

	public void removeEdge(String from, String to) throws WorkflowException {
		checkNotNull(from);
		EdgeEntity found = this.edgeRepository.findFirstByFromAndTo(from, to);
		if(found != null) {
			this.edgeRepository.delete(found);
		} else {
			throw new WorkflowException(String.format("There exists no edge from %s to %s", from, to));
		}
	}
}
