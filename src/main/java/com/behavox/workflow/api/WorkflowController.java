package com.behavox.workflow.api;

import com.behavox.workflow.api.converter.StateConverter;
import com.behavox.workflow.api.model.CreateEdgeRequest;
import com.behavox.workflow.api.model.DeleteEdgeRequest;
import com.behavox.workflow.api.model.StateApiModel;

import com.behavox.workflow.service.WorkflowException;
import com.behavox.workflow.service.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.behavox.workflow.utils.Util.toErrorResponse;

@RestController
public class WorkflowController {

	private static final Logger logger = LoggerFactory.getLogger(WorkflowController.class);

	private static final StateConverter STATE_CONVERTER = new StateConverter();

	@Autowired
	private WorkflowService workflowService;

	@GetMapping("/workflow")
	public ResponseEntity getWorkflow(){
		try {
			List<StateApiModel> states = workflowService.getWorkFlow()
					.stream()
					.map(STATE_CONVERTER)
					.collect(Collectors.toList());

			return ResponseEntity.ok(states);
		}  catch (Exception ex){
			logger.error("Unknown server exception", ex);
			return toErrorResponse(ex.getMessage(), 500);
		}
	}

	@PostMapping("/workflow/edge")
	public ResponseEntity addNewEdge(@RequestBody CreateEdgeRequest createEdgeRequest) {
		try {
			workflowService.addEdge(
					createEdgeRequest.getFrom(),
					createEdgeRequest.getTo(),
					createEdgeRequest.getName(),
					createEdgeRequest.getAction()
			);
		} catch (WorkflowException we){
			logger.error("Could not add new edge.", we);
			return toErrorResponse(we.getMessage(), 400);
		} catch (Exception ex){
			logger.error("Unknown server exception", ex);
			return toErrorResponse(ex.getMessage(), 500);
		}
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/workflow/state/{name}")
	public ResponseEntity deleteState(@PathVariable String name) {
		try {
			workflowService.removeState(name);
		} catch (WorkflowException we){
			logger.error("Could not delete state.", we);
			return toErrorResponse(we.getMessage(), 400);
		} catch (Exception ex){
			logger.error("Unknown server exception", ex);
			return toErrorResponse(ex.getMessage(), 500);
		}
		return ResponseEntity.ok().build();
	}

	@PostMapping("/workflow/state/{name}")
	public ResponseEntity addState(@PathVariable String name) {
		try {
			workflowService.addState(name);
		} catch (WorkflowException we){
			logger.error("Could not add new state.", we);
			return toErrorResponse(we.getMessage(), 400);
		} catch (Exception ex){
			logger.error("Unknown server exception", ex);
			return toErrorResponse(ex.getMessage(), 500);
		}
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/workflow/edge")
	public ResponseEntity deleteEdge(@RequestBody DeleteEdgeRequest deleteEdgeRequest) {
		try {
			workflowService.removeEdge(deleteEdgeRequest.getFrom(), deleteEdgeRequest.getTo());
		} catch (WorkflowException we){
			logger.error("Could delete an edge.", we);
			return toErrorResponse(we.getMessage(), 400);
		} catch (Exception ex){
			logger.error("Unknown server exception", ex);
			return toErrorResponse(ex.getMessage(), 500);
		}
		return ResponseEntity.ok().build();
	}

}
