package com.behavox.workflow.api;

import com.behavox.workflow.api.converter.IssueConverter;
import com.behavox.workflow.api.converter.UserIssueConverter;
import com.behavox.workflow.api.model.IssueApiModel;
import com.behavox.workflow.api.model.MoveIssueResponse;
import com.behavox.workflow.api.model.UserIssueApiModel;
import com.behavox.workflow.model.IssueEntity;
import com.behavox.workflow.service.IssueService;
import com.behavox.workflow.service.MoveIssueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class IssueController {

	private static final Logger logger = LoggerFactory.getLogger(IssueController.class);

	@Autowired
	private IssueService issueService;

	private static final UserIssueConverter USER_ISSUE_CONVERTER = new UserIssueConverter();
	private static final IssueConverter ISSUE_CONVERTER = new IssueConverter();

	@GetMapping("/issues")
	public ResponseEntity getAllIssuesByUsers(){

		List<UserIssueApiModel> userIssue = this.issueService
				.getAllUserIssue()
				.stream()
				.map(USER_ISSUE_CONVERTER)
				.collect(Collectors.toList());

		return ResponseEntity.ok(userIssue);
	}

	@PostMapping("/issues/{issueId}/transition/{state}")
	public ResponseEntity moveIssue(@PathVariable long issueId, @PathVariable String state){
		IssueEntity entity;
		try {

			entity = this.issueService.moveIssueToAnotherState(issueId, state);
			IssueApiModel issueApiModel = ISSUE_CONVERTER.apply(entity);
			MoveIssueResponse moveIssueResponse = new MoveIssueResponse(issueApiModel, state);
			return ResponseEntity.ok(moveIssueResponse);

		} catch (MoveIssueException mie) {
			logger.error("Could not move issue to a new state", mie);
			ErrorResponse response = new ErrorResponse(mie.getMessage());
			return ResponseEntity.status(400).body(response);
		}
	}
}
