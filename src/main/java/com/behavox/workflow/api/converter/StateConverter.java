package com.behavox.workflow.api.converter;

import com.behavox.workflow.api.model.EdgeApiModel;
import com.behavox.workflow.api.model.StateApiModel;

import com.behavox.workflow.model.StateEntity;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StateConverter implements Function<StateEntity, StateApiModel> {

	private static final EdgeConverter EDGE_CONVERTER = new EdgeConverter();

	@Override
	public StateApiModel apply(StateEntity stateEntity) {
		List<EdgeApiModel> outgoing = stateEntity.getOutgoingEdgeEntities()
				.stream()
				.map(EDGE_CONVERTER)
				.collect(Collectors.toList());

		List<EdgeApiModel> incoming = stateEntity.getIncomingEdgeEntities()
				.stream()
				.map(EDGE_CONVERTER)
				.collect(Collectors.toList());

		return new StateApiModel(stateEntity.getName(), outgoing, incoming);
	}
}
