package com.behavox.workflow.api.converter;

import com.behavox.workflow.api.model.IssueApiModel;
import com.behavox.workflow.model.IssueEntity;

import java.util.function.Function;

public class IssueConverter implements Function<IssueEntity, IssueApiModel> {

	@Override
	public IssueApiModel apply(IssueEntity issueEntity) {
		return new IssueApiModel(issueEntity.getId(), issueEntity.getTitle(), issueEntity.getState());
	}
}
