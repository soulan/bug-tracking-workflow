package com.behavox.workflow.api.converter;

import com.behavox.workflow.api.model.EdgeApiModel;
import com.behavox.workflow.model.EdgeEntity;

import java.util.function.Function;

public class EdgeConverter implements Function<EdgeEntity, EdgeApiModel> {

	@Override
	public EdgeApiModel apply(EdgeEntity edgeEntity) {
		return new EdgeApiModel(edgeEntity.getFrom(), edgeEntity.getTo(), edgeEntity.getName(), edgeEntity.getAction());
	}
}
