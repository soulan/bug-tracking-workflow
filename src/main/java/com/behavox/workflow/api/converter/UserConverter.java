package com.behavox.workflow.api.converter;

import com.behavox.workflow.api.model.UserApiModel;
import com.behavox.workflow.model.UserEntity;

import java.util.function.Function;

public class UserConverter implements Function<UserEntity, UserApiModel> {

	@Override
	public UserApiModel apply(UserEntity userEntity) {
		return new UserApiModel(userEntity.getId(), userEntity.getName());
	}
}
