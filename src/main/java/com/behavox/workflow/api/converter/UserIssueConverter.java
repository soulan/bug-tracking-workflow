package com.behavox.workflow.api.converter;

import com.behavox.workflow.api.model.IssueApiModel;
import com.behavox.workflow.api.model.UserApiModel;
import com.behavox.workflow.api.model.UserIssueApiModel;
import com.behavox.workflow.model.IssueEntity;
import com.behavox.workflow.model.UserEntity;
import com.behavox.workflow.model.UserIssueEntity;

import java.util.function.Function;

public class UserIssueConverter implements Function<UserIssueEntity, UserIssueApiModel> {

	private static final IssueConverter ISSUE_CONVERTER = new IssueConverter();
	private static final UserConverter USER_CONVERTER = new UserConverter();

	@Override
	public UserIssueApiModel apply(UserIssueEntity userIssueEntity) {
		UserEntity userEntity = userIssueEntity.getUserEntity();
		IssueEntity issueEntity = userIssueEntity.getIssueEntity();

		UserApiModel userApiModel = USER_CONVERTER.apply(userEntity);
		IssueApiModel issueApiModel = ISSUE_CONVERTER.apply(issueEntity);

		return new UserIssueApiModel(userApiModel, issueApiModel);
	}
}
