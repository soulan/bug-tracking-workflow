package com.behavox.workflow.api.model;

public class MoveIssueResponse {

	private IssueApiModel issue;
	private String previousState;

	public MoveIssueResponse() {
	}

	public MoveIssueResponse(IssueApiModel issue, String previousState) {
		this.issue = issue;
		this.previousState = previousState;
	}

	public IssueApiModel getIssue() {
		return issue;
	}

	public void setIssue(IssueApiModel issue) {
		this.issue = issue;
	}

	public String getPreviousState() {
		return previousState;
	}

	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}
}
