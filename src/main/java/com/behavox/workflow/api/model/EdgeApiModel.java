package com.behavox.workflow.api.model;

public class EdgeApiModel {

	private String from;
	private String to;
	private String name;
	private String action;

	public EdgeApiModel(String from, String to, String name, String action) {
		this.from = from;
		this.to = to;
		this.name = name;
		this.action = action;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
