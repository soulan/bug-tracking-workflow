package com.behavox.workflow.api.model;

public class UserIssueApiModel {

	private UserApiModel user;
	private IssueApiModel issue;

	public UserIssueApiModel(UserApiModel user, IssueApiModel issue) {
		this.user = user;
		this.issue = issue;
	}

	public UserApiModel getUser() {
		return user;
	}

	public void setUser(UserApiModel user) {
		this.user = user;
	}

	public IssueApiModel getIssue() {
		return issue;
	}

	public void setIssue(IssueApiModel issue) {
		this.issue = issue;
	}
}
