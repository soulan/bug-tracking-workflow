package com.behavox.workflow.api.model;

public class IssueApiModel {
	private long id;
	private String title;
	private String state;

	public IssueApiModel(long id, String title, String state) {
		this.id = id;
		this.title = title;
		this.state = state;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
