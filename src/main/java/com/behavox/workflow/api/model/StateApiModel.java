package com.behavox.workflow.api.model;

import java.util.List;

public class StateApiModel {

	private String name;
	private List<EdgeApiModel> outgoingEdges;
	private List<EdgeApiModel> incomingEdges;

	public StateApiModel(String name, List<EdgeApiModel> outgoingEdges, List<EdgeApiModel> incomingEdges) {
		this.name = name;
		this.outgoingEdges = outgoingEdges;
		this.incomingEdges = incomingEdges;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EdgeApiModel> getOutgoingEdges() {
		return outgoingEdges;
	}

	public void setOutgoingEdges(List<EdgeApiModel> outgoingEdges) {
		this.outgoingEdges = outgoingEdges;
	}

	public List<EdgeApiModel> getIncomingEdges() {
		return incomingEdges;
	}

	public void setIncomingEdges(List<EdgeApiModel> incomingEdges) {
		this.incomingEdges = incomingEdges;
	}
}
