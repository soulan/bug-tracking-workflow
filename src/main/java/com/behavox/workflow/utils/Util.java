package com.behavox.workflow.utils;

import com.behavox.workflow.api.ErrorResponse;
import org.springframework.http.ResponseEntity;

public final class Util {
	public static ResponseEntity toErrorResponse(String message, int httpStatus) {
		ErrorResponse response = new ErrorResponse(message);
		return ResponseEntity.status(httpStatus).body(response);
	}
}
