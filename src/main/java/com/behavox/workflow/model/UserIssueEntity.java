package com.behavox.workflow.model;

import javax.persistence.*;

@Entity
@Table(name = "t_user_issue")
@IdClass(UserIssuePk.class)
public class UserIssueEntity {

	@Id
	@Column(name= "issue_pk")
	private long issuePk;

	@Id
	@Column(name= "user_pk")
	private long userPk;

	@ManyToOne
	@JoinColumn(name = "issue_pk", insertable = false, updatable = false)
	private IssueEntity issueEntity;

	@ManyToOne
	@JoinColumn(name = "user_pk", insertable = false, updatable = false)
	private UserEntity userEntity;

	public UserIssueEntity() {
	}

	public UserIssueEntity(long issuePk, long userPk, IssueEntity issueEntity, UserEntity userEntity) {
		this.issuePk = issuePk;
		this.userPk = userPk;
		this.issueEntity = issueEntity;
		this.userEntity = userEntity;
	}

	public long getIssuePk() {
		return issuePk;
	}

	public void setIssuePk(long issuePk) {
		this.issuePk = issuePk;
	}

	public long getUserPk() {
		return userPk;
	}

	public void setUserPk(long userPk) {
		this.userPk = userPk;
	}

	public IssueEntity getIssueEntity() {
		return issueEntity;
	}

	public void setIssueEntity(IssueEntity issueEntity) {
		this.issueEntity = issueEntity;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
}
