package com.behavox.workflow.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_issue")
public class IssueEntity {

	@Id
	@Column(name = "issue_id")
	private long id;

	@Column
	private String title;

	@Column
	private String state;

	public IssueEntity() {
	}

	public IssueEntity(long id, String title, String state) {
		this.id = id;
		this.title = title;
		this.state = state;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
