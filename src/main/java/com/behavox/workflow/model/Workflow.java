package com.behavox.workflow.model;

import java.util.List;

public class Workflow {

	private List<StateEntity> states;

	public Workflow(List<StateEntity> states) {
		this.states = states;
	}

	public List<StateEntity> getStates() {
		return states;
	}

	public void setStates(List<StateEntity> states) {
		this.states = states;
	}

}
