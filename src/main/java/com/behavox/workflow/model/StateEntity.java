package com.behavox.workflow.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_state")
public class StateEntity {

	public StateEntity() {
	}

	public StateEntity(String name, List<EdgeEntity> outgoingEdgeEntities, List<EdgeEntity> incomingEdgeEntities) {
		this.name = name;
		this.outgoingEdgeEntities = outgoingEdgeEntities;
		this.incomingEdgeEntities = incomingEdgeEntities;
	}

	@Id
	@Column(unique = true)
	private String name;

	@OneToMany
	@JoinColumn(name = "from_state")
	private List<EdgeEntity> outgoingEdgeEntities;

	@OneToMany
	@JoinColumn(name = "to_state")
	private List<EdgeEntity> incomingEdgeEntities;

	public List<EdgeEntity> getOutgoingEdgeEntities() {
		return outgoingEdgeEntities;
	}

	public void setOutgoingEdgeEntities(List<EdgeEntity> outgoingEdgeEntities) {
		this.outgoingEdgeEntities = outgoingEdgeEntities;
	}

	public List<EdgeEntity> getIncomingEdgeEntities() {
		return incomingEdgeEntities;
	}

	public void setIncomingEdgeEntities(List<EdgeEntity> incomingEdgeEntities) {
		this.incomingEdgeEntities = incomingEdgeEntities;
	}

	public StateEntity(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
