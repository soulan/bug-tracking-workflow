package com.behavox.workflow.model;

import javax.persistence.*;

@Entity
@Table(name = "t_edge")
@IdClass(EdgePK.class)
public class EdgeEntity {

	@Id
	@Column(name = "from_state")
	private String from;

	@Id
	@Column(name = "to_state")
	private String to;

	@Column(name = "name")
	private String name;

	@Column(name = "action")
	private String action;

	public EdgeEntity() {

	}

	public EdgeEntity(String from, String to, String name, String action) {
		this.from = from;
		this.to = to;
		this.name = name;
		this.action = action;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
