package com.behavox.workflow.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_user")
public class UserEntity {

	@Id
	@Column(name = "user_id")
	private long id;

	@Column
	private String name;

	public UserEntity(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public UserEntity() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
