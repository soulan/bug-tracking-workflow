package com.behavox.workflow.model;

import java.io.Serializable;

public class UserIssuePk implements Serializable {

	private long userPk;
	private long issuePk;


	public UserIssuePk() {
	}

	public long getIssuePk() {
		return issuePk;
	}

	public void setIssuePk(long issuePk) {
		this.issuePk = issuePk;
	}

	public long getUserPk() {
		return userPk;
	}

	public void setUserPk(long userPk) {
		this.userPk = userPk;
	}
}
