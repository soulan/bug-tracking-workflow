package com.behavox.workflow.persist;

import com.behavox.workflow.model.UserIssueEntity;
import com.behavox.workflow.model.UserIssuePk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserIssueRepository extends JpaRepository<UserIssueEntity, UserIssuePk> {

}
