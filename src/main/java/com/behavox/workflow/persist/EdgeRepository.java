package com.behavox.workflow.persist;

import com.behavox.workflow.model.EdgeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EdgeRepository extends JpaRepository<EdgeEntity, String> {
	EdgeEntity findFirstByFromAndTo(String from, String to);
}
