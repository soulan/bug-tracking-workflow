package com.behavox.workflow.persist;

import com.behavox.workflow.model.IssueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueRepository extends JpaRepository<IssueEntity, Long> {
}
