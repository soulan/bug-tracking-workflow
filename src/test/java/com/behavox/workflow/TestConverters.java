package com.behavox.workflow;

import com.behavox.workflow.api.converter.*;
import com.behavox.workflow.api.model.*;
import com.behavox.workflow.model.*;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
public class TestConverters {

    private IssueConverter issueConverter = new IssueConverter();
    private EdgeConverter edgeConverter = new EdgeConverter();
    private StateConverter stateConverter = new StateConverter();
    private UserConverter userConverter = new UserConverter();
    private UserIssueConverter userIssueConverter = new UserIssueConverter();

    @Test
    public void testIssueConverter(){

        final long id = 1;
        final String title = "Cannot find my lunch";
        final String state = "closed";

        IssueEntity issueEntity = new IssueEntity(id, title, state);
        IssueApiModel issueApiModel = issueConverter.apply(issueEntity);

        assertEquals(id, issueApiModel.getId());
        assertEquals(title, issueApiModel.getTitle());
        assertEquals(state, issueApiModel.getState());
    }

    @Test
    public void testEdgeConverter(){
        final String from = "tom";
        final String to = "jerry";
        final String name = "revive";
        final String action = "chase";

        EdgeEntity edgeEntity = new EdgeEntity(from, to, name, action);
        EdgeApiModel edgeApiModel = edgeConverter.apply(edgeEntity);

        assertEquals(from, edgeApiModel.getFrom());
        assertEquals(to, edgeApiModel.getTo());
        assertEquals(name, edgeApiModel.getName());
        assertEquals(action, edgeApiModel.getAction());
    }

    @Test
    public void testStateConverter(){
        final String state = "stateless";
        StateEntity stateEntity = new StateEntity(state, Collections.emptyList(), Collections.emptyList());
        StateApiModel stateApiModel = stateConverter.apply(stateEntity);

        assertEquals(state, stateApiModel.getName());
        assertEquals(Collections.emptyList(), stateApiModel.getIncomingEdges());
        assertEquals(Collections.emptyList(), stateApiModel.getOutgoingEdges());
    }

    @Test
    public void testUserConverter(){
        final long userId = 0;
        final String name = "tom";

        UserEntity userEntity = new UserEntity(userId, name);
        UserApiModel userApiModel = userConverter.apply(userEntity);

        assertEquals(userId, userApiModel.getId());
        assertEquals(name, userApiModel.getName());
    }

    @Test
    public void testUserIssueConverter(){
        final long issueId = 0;
        final long userId = 1;
        final String name = "dude";
        final String title = "some title";
        final String state = "some state";

        UserEntity userEntity = new UserEntity(userId, name);
        IssueEntity issueEntity = new IssueEntity(issueId, title, state);
        UserIssueEntity userIssueEntity = new UserIssueEntity(
                userEntity.getId(),
                issueEntity.getId(),
                issueEntity,
                userEntity
        );

        UserIssueApiModel userIssueApiModel = userIssueConverter.apply(userIssueEntity);
        assertEquals(userId, userIssueApiModel.getUser().getId());
        assertEquals(name, userIssueApiModel.getUser().getName());
        assertEquals(issueId, userIssueApiModel.getIssue().getId());
        assertEquals(title, userIssueApiModel.getIssue().getTitle());
        assertEquals(state, userIssueApiModel.getIssue().getState());
    }
}
