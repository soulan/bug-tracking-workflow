# README #

Workflow service

### Requirements

- MySQL:8.0.15  

### Running the project

#### Test
`gradle clean test`

#### Build
`gradle clean build`

#### Run
`java -jar workflow-service-1.0.0.jar`

## Configurations / Environment variables

```
server.port=8080
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/workflow
spring.datasource.username=YOUR-DB-USERNAME
spring.datasource.password=YOUR-DB-PWD
spring.flyway.user=YOUR-DB-USERNAME
spring.flyway.password=YOUR-DB-PWD
spring.flyway.enabled=true
spring.flyway.baseline-on-migrate=true

```

# RestAPI Endpoints

## Workflow Management:

### Add a state:
POST `/workflow/state/{name}`

### Remove a state:
DELETE `/workflow/state/{name}`

### Add an edge:

POST `/workflow/edge`

Request
---
```json
{
  "name" : "close",
  "from": "open",
  "to": "closed",
  "action": "do something"
}
```

### Remove an edge:

DELETE `/workflow/edge`

Request
---
```json
{
    "from": "open",
    "to": "closed"
}
```

## Issue Management

## Move Issue to another state:

POST `/issues/{issueId}/transition/{state}`

## Get all issues per user:

GET `/issues`

